## 主体项目

* yudao-ui-admin：管理后台的前端
* yudao-ui-user：用户前台的前端
* yudao-server：提供前端 RESTful API 的服务端

## 模块化

按照 yudao-module-xxx 命名，每个模块包含两个子工程：
* yudao-module-xxx-api：暴露给其它模块的 API 定义；
* yudao-module-xxx-impl：实现该模块的用户前台的 RESTful API，管理后台的 RESTful API，暴露给其它模块的 API，自己的 Service、DAO、Job、MQ 等实现；

为什么要有 yudao-module-xxx-api 呢？明确暴露给其它 Module 的 API，避免它们去调用内部逻辑，也算是未来迁移微服务的基础。  
因此，其它 yudao-module-yyy-impl 模块想要调用 yudao-module-xxx-impl 时，引入的是 yudao-module-xxx-api。这样，它就无法感知到未暴露出来的逻辑，实体等等。
最终，我们会在 yudao-server 项目中，把所有的 yudao-module-***-impl 引入，进行启动，提供出去。

## yudao-module-xxx-api

* enums
* api
  * 包：暴露给其它模块的 API 的具体的定义
    * dto：数据传输

## yudao-module-xxx-impl

和之前的差不多，只是多了 3 个包
* controlleruser：提供给用户前台的 RESTful API
* controlleradmin：提供给前台的 RESTful API
* api：提供暴露给其它模块的 API 的具体实现

最终的调用的链路是：

API=》Service=》DAO，其中 API 出入参都是 DTO

Controller=》Service=》DAO，其中 Controller 出入参都是 VO
* 因为有多种 Controller，所以会有存在同名的情况，无可避免，包括 Controller 也是。

另外，Service 考虑简单，直接返回 DO。如果需要自定义对象，可以使用 BO