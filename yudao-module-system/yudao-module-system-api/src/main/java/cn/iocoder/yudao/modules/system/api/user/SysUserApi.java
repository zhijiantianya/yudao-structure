package cn.iocoder.yudao.modules.system.api.user;

import cn.iocoder.yudao.modules.system.api.user.dto.SysUserRespDTO;

/**
 * User API 接口
 *
 * @author 芋道源码
 */
public interface SysUserApi {

    SysUserRespDTO getUser(Long id);

}
