package cn.iocoder.yudao.modules.system.api.user;

import cn.iocoder.yudao.modules.system.api.user.dto.SysUserRespDTO;
import cn.iocoder.yudao.modules.system.convert.user.SysUserConvert;
import cn.iocoder.yudao.modules.system.dal.dataobject.user.SysUserDO;
import cn.iocoder.yudao.modules.system.service.user.SysUserService;

public class SysUserApiImpl implements SysUserApi {

    private SysUserService userService;

    @Override
    public SysUserRespDTO getUser(Long id) {
        SysUserDO user = userService.getUser(id);
        return SysUserConvert.INSTANCE.convert(user);
    }

}
