package cn.iocoder.yudao.modules.system.dal.mysql.user;

import cn.iocoder.yudao.modules.system.dal.dataobject.user.SysUserDO;

public interface SysUserMapper {

    SysUserDO selectById(Long id);

}
