package cn.iocoder.yudao.modules.system.convert.user;

import cn.iocoder.yudao.modules.system.api.user.dto.SysUserRespDTO;
import cn.iocoder.yudao.modules.system.dal.dataobject.user.SysUserDO;

public interface SysUserConvert {

    SysUserConvert INSTANCE = null;

    SysUserRespDTO convert(SysUserDO bean);

}
