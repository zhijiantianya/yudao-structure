package cn.iocoder.yudao.modules.system.service.user;

import cn.iocoder.yudao.modules.system.dal.dataobject.user.SysUserDO;

/**
 * User Service 接口
 */
public interface SysUserService {

    SysUserDO getUser(Long id);

}
