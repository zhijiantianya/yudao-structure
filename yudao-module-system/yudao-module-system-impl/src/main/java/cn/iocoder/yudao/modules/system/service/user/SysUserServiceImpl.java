package cn.iocoder.yudao.modules.system.service.user;

import cn.iocoder.yudao.modules.system.dal.dataobject.user.SysUserDO;
import cn.iocoder.yudao.modules.system.dal.mysql.user.SysUserMapper;

public class SysUserServiceImpl implements SysUserService {

    private SysUserMapper userMapper;

    @Override
    public SysUserDO getUser(Long id) {
        return userMapper.selectById(id);
    }

}
